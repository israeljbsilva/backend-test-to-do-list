from rest_framework import serializers
from .models import ToDoList


class ToDoListSerializer(serializers.ModelSerializer):

    class Meta:
        model = ToDoList
        fields = ('id', 'title', 'description', 'status', 'dealine', 'user')
        read_only_fields = ('id', )


class ReportSerializer(serializers.Serializer):
    initial_date = serializers.DateTimeField(input_formats='%Y-%m-%d')
    final_date = serializers.DateTimeField(input_formats='%Y-%m-%d')
