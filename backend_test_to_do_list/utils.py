def write_pdf_report(file_name, pdf_report):
    with open(file_name, 'wb') as f:
        f.write(pdf_report)


def group_to_do_list_for_same_user(to_do_lists):
    to_do_lists_users = {}
    for to_do_list in to_do_lists:
        if to_do_list.user.email not in to_do_lists_users:
            to_do_lists_users[to_do_list.user.email] = [to_do_list]
        else:
            to_do_lists_users[to_do_list.user.email].append(to_do_list)
    return to_do_lists_users
