"""backend_test_to_do_list URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path, include, re_path
from rest_framework_nested import routers

from .views import ToDoListViewSet, ReportViewSet

from rest_framework_simplejwt import views as jwt_views
from rest_auth.views import LoginView, PasswordResetView, PasswordResetConfirmView, UserDetailsView
from rest_auth.registration.views import RegisterView


app_name = 'backend_test_to_do_list'


to_do_list_router = routers.DefaultRouter(trailing_slash=False)
to_do_list_router.register(r'to-do-list', ToDoListViewSet)

report_router = routers.DefaultRouter(trailing_slash=False)
report_router.register(r'report', ReportViewSet)


urlpatterns = [
    path('', include(to_do_list_router.urls)),
    path('', include(report_router.urls)),
    path('user', UserDetailsView.as_view(), name='user'),
    path('user/auth', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('user/create-account', RegisterView().as_view(), name='create_account'),
    path('user/login', LoginView().as_view(), name='login'),
    path('user/password-reset', PasswordResetView().as_view(), name='password_reset'),
    path('user/password-reset-confirm', PasswordResetConfirmView().as_view(), name='password_reset_confirm'),
    path(r'accounts/', include('allauth.urls')),
]
