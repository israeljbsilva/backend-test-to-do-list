from django.apps import AppConfig


class BackendTestToDoListConfig(AppConfig):
    name = 'backend_test_to_do_list'
