import datetime
import sys

from xhtml2pdf import pisa

from io import BytesIO

from background_task import background
from background_task.tasks import Task
from background_task.models_completed import CompletedTask

from celery import shared_task
from celery.utils.log import get_task_logger

from django.conf import settings
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.contrib.auth.models import User

from .models import ToDoList
from .utils import write_pdf_report, group_to_do_list_for_same_user


logger = get_task_logger(__name__)
logger.info(__name__)


@shared_task
def generate_pdf_report(to_do_lists, title):
    rendered = render_to_string('pdf_report.html', {'to_do_lists': to_do_lists, 'title': title})
    pdf_report = BytesIO()
    pisa.pisaDocument(BytesIO(rendered.encode("UTF-8")), pdf_report)
    return pdf_report.getvalue()


@shared_task
def send_email_with_pdf_report(file, subject, body, to):
    logger.info('E-mail processing has been started.')
    message = EmailMessage(subject, body=body, from_email=settings.EMAIL_HOST_USER, to=[to])
    write_pdf_report('pdf_report.pdf', file)
    message.attach_file('pdf_report.pdf')
    message.send()
    logger.info('E-mail processing has been finished.')


@background()
def task_get_completed_tasks():
    today = datetime.datetime.today().strftime('%Y-%m-%d')
    completed_tasks = ToDoList.objects.filter(status='COMPLETED', dealine=today)
    completed_tasks_users = group_to_do_list_for_same_user(completed_tasks)
    for email_completed_tasks in completed_tasks_users:
        pdf_report_completed_tasks = generate_pdf_report(
            completed_tasks_users[email_completed_tasks], title='Relatório diário das tarefas concluídas')
        send_email_with_pdf_report(
            file=pdf_report_completed_tasks,
            subject='[Relatório de Tarefas Diário - Tarefas Concluídas]',
            body='Segue anexo o relatório diário.',
            to=email_completed_tasks)


@background()
def task_get_next_task_priorities():
    next_task_priorities = ToDoList.objects.filter(status='CREATED').order_by('dealine')
    next_task_priorities_users = group_to_do_list_for_same_user(next_task_priorities)
    for email_next_task_priorities in next_task_priorities_users:
        pdf_report_next_task_priorities = generate_pdf_report(
            next_task_priorities_users[email_next_task_priorities], title='Relatório diário das próximas prioridades.')
        send_email_with_pdf_report(
            file=pdf_report_next_task_priorities,
            subject='[Relatório de Tarefas Diário - Próximas Prioridades]',
            body='Segue anexo o relatório diário.',
            to=email_next_task_priorities)


def _set_verbose_name(verbose_name):
    users = User.objects.all()
    for user in users:
        return f'{verbose_name}:{user.email}'


def _completed_tasks(tasks_name):
    today = datetime.datetime.today()
    tasks = CompletedTask.objects.filter(
        verbose_name__in=tasks_name, run_at__year=today.year, run_at__month=today.month, run_at__day=today.day)
    if not tasks:
        return False
    return True


if 'process_tasks' in sys.argv:  # pragma: no cover
    if not _completed_tasks((_set_verbose_name('task_get_completed_tasks'),
                             _set_verbose_name('task_get_next_task_priorities'))):
        task_get_completed_tasks(
            repeat=Task.DAILY, verbose_name=_set_verbose_name('task_get_completed_tasks'))
        task_get_next_task_priorities(
            repeat=Task.DAILY, verbose_name=_set_verbose_name('task_get_next_task_priorities'))
