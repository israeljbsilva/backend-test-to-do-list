import datetime
import uuid
import factory

from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User

from backend_test_to_do_list.models import ToDoList
from backend_test_to_do_list.enums import ToDoListStatus


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    username = factory.Faker('email')
    password = factory.LazyFunction(lambda: make_password('123456'))
    is_staff = True
    is_superuser = True


class ToDoListFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ToDoList

    id = uuid.uuid4()
    title = 'Fazer faxina'
    description = 'Esfregar a parede'
    status = ToDoListStatus.created
    dealine = datetime.datetime.now().date()
    user = factory.SubFactory(UserFactory)
