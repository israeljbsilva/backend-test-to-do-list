from djchoices import DjangoChoices, ChoiceItem


class ToDoListStatus(DjangoChoices):
    created = ChoiceItem('CREATED')
    completed = ChoiceItem('COMPLETED')
