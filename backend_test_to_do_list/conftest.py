import pytest

from pytest_factoryboy import register

from .factories import ToDoListFactory, UserFactory


register(ToDoListFactory)
register(UserFactory)


@pytest.fixture
def customer_client(db):
    from django.test.client import Client
    return Client()
