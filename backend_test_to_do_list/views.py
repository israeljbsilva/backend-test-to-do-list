import os

from http import HTTPStatus

from django.http import JsonResponse
from django.utils import timezone

from django.db.models import Q

from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from .models import ToDoList
from .serializers import ToDoListSerializer, ReportSerializer
from .pagination import DefaultPagination
from .tasks import send_email_with_pdf_report, generate_pdf_report
from .utils import group_to_do_list_for_same_user


def ping(request):
    return JsonResponse({
        'request': f'{request.environ.get("REQUEST_METHOD")} '
        f'{request.environ.get("HTTP_HOST")}{request.environ.get("PATH_INFO")}',
        'timestamp': timezone.localtime(),
        'build_date': os.environ.get('BUILD_DATE'),
        'revision': os.environ.get('REVISION')})


class ToDoListViewSet(viewsets.ModelViewSet):
    queryset = ToDoList.objects.all()
    serializer_class = ToDoListSerializer
    pagination_class = DefaultPagination


class ReportViewSet(viewsets.ModelViewSet):
    queryset = ToDoList.objects.all()
    serializer_class = ReportSerializer
    pagination_class = DefaultPagination
    http_method_names = ['post']
    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        initial_date = request.data.get('initial_date')
        final_date = request.data.get('final_date')
        filters = (Q())
        filters.add(Q(dealine__gte=initial_date), Q.AND)
        filters.add(Q(dealine__lte=final_date), Q.AND)
        to_do_lists = self.paginator.paginate_queryset(self.queryset.filter(filters), request)
        if to_do_lists:
            messages = []
            subject = f'[Relatório de Tarefas (Assíncrono)] {initial_date} á {final_date}'
            body = f'Segue anexo o relatório de tarefas entre as datas {initial_date} e {final_date}.'
            to_do_lists_users = group_to_do_list_for_same_user(to_do_lists)
            for email in to_do_lists_users:
                pdf_report = generate_pdf_report(to_do_lists_users[email], title=subject)
                send_email_with_pdf_report(file=pdf_report, subject=subject, body=body, to=email)
                messages.append({'message': 'Email with PDF report sent.', 'e-mail': email})
            return Response(messages, HTTPStatus.OK)
        return Response({'message': 'No to do list found.'}, HTTPStatus.NOT_FOUND)
