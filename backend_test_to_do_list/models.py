import uuid

from django.db import models
from django.db.models.fields import CharField, UUIDField
from django.conf import settings

from django_extensions.db.models import TimeStampedModel

from .enums import ToDoListStatus


class ToDoList(TimeStampedModel, models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, serialize=False, editable=False, unique=True)
    title = CharField('TITLE', max_length=256, null=False)
    description = CharField('NAME', max_length=256, null=False)
    status = CharField(
        'STATUS', max_length=32, choices=ToDoListStatus.choices, default=ToDoListStatus.created, null=False)
    dealine = models.DateField('DEADLINE', null=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    class Meta:
        db_table = 'ToDoList'
        verbose_name = 'to_do_list'
        verbose_name_plural = 'to_do_lists'
