import uuid
import pytest

from backend_test_to_do_list.serializers import ToDoListSerializer
from backend_test_to_do_list.enums import ToDoListStatus


pytestmark = [pytest.mark.django_db, pytest.mark.serial]


def test_should_serialize_to_do_list(to_do_list):
    # GIVEN
    # WHEN
    serializer = ToDoListSerializer(to_do_list)

    # THEN
    assert isinstance(serializer.data, dict)
    assert serializer.data.get('id') is not None
    assert serializer.data.get('title') == 'Fazer faxina'
    assert serializer.data.get('description') == 'Esfregar a parede'
    assert serializer.data.get('status') == ToDoListStatus.created
    assert serializer.data.get('user')


def test_should_deserialize_to_do_list(user):
    # GIVEN
    to_do_list_id = uuid.uuid4()
    to_do_list_data = {
        'id': to_do_list_id,
        'title': 'Fazer faxina',
        'description': 'Esfregar a parede',
        'status': ToDoListStatus.created,
        'dealine': '2019-07-14',
        'user': user.id
    }

    # WHEN
    serializer = ToDoListSerializer(data=to_do_list_data)

    # THEN
    assert serializer.is_valid()

    to_do_list = serializer.save(id=to_do_list_id)
    assert to_do_list.id
    assert to_do_list.title == 'Fazer faxina'
    assert to_do_list.description == 'Esfregar a parede'
    assert to_do_list.status == ToDoListStatus.created
    assert to_do_list.user.id == user.id
