import json

from http import HTTPStatus

from backend_test_to_do_list.enums import ToDoListStatus


RESOURCE_PATH = '/api/v1'


def test_should_create_to_do_list(customer_client, user):
    # GIVEN
    to_do_list_data = {
        'title': 'Fazer faxina',
        'description': 'Esfregar a parede',
        'status': ToDoListStatus.created,
        'dealine': '2019-07-14',
        'user': user.id
    }

    # WHEN
    response = customer_client.post(
        f'{RESOURCE_PATH}/to-do-list', data=json.dumps(to_do_list_data), content_type='application/json')

    # THEN
    assert response.status_code == HTTPStatus.CREATED

    content = json.loads(response.content.decode())
    assert content.get('id') is not None
    assert content.get('title') == 'Fazer faxina'
    assert content.get('description') == 'Esfregar a parede'
    assert content.get('status') == ToDoListStatus.created
    assert content.get('dealine') == '2019-07-14'
    assert content.get('user')


def test_should_not_create_title_to_do_list(customer_client):
    # GIVEN
    to_do_list_data = {}

    # WHEN
    response = customer_client.post(
        f'{RESOURCE_PATH}/to-do-list', data=json.dumps(to_do_list_data), content_type='application/json')

    # THEN
    assert response.status_code == HTTPStatus.BAD_REQUEST

    content = json.loads(response.content.decode())
    assert content == {
        'title': ['Este campo é obrigatório.'],
        'description': ['Este campo é obrigatório.'],
        'dealine': ['Este campo é obrigatório.'],
        'user': ['Este campo é obrigatório.'],
    }
