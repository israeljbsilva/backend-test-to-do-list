import pytest

from backend_test_to_do_list.models import ToDoList
from backend_test_to_do_list.enums import ToDoListStatus


pytestmark = [pytest.mark.django_db, pytest.mark.serial]


def test_should_create_to_do_list(to_do_list, user):
    assert ToDoList.objects.count() == 1
    assert to_do_list.id is not None
    assert to_do_list.title == 'Fazer faxina'
    assert to_do_list.description == 'Esfregar a parede'
    assert to_do_list.status == ToDoListStatus.created
    assert to_do_list.user.id == user.id
