# backend-test-to-do-list

- 1- CRUD de TODO Lists: criar, remover, editar o título;
- 2- CRUD de tarefas: para cada TODO List, possibilidade de criar tarefas, editar, remover, marcar com concluída e adicionar uma deadline;
- 3- Possibilidade de atribuir uma tarefa para um usuário;
- 4- Relatório diário via e-mail de tarefas concluídas naquele dia e próximas prioridades, baseado no deadline;
- 5- Possibilidade de solicitar um relatório, enviando a data de início e fim, em um endpoint REST com autenticação utilizando token. A resposta do endpoint deve ser imediata, informando se é possível a geração do relatório e, se sim, para qual e-mail será enviado. A geração e envio via e-mail deve acontecer de forma assíncrona (em background);
- 6- Possibilidade de fazer login no usuário admin@biome-hub.com, onde seu password será xxyyzz onde xx será o dobro do dia atual, yy será o dobro do mês e zz a hora atual (sempre 6 caracteres);

### Build e Testes
<a href="https://gitlab.com/israeljbsilva/backend-test-to-do-list/commits/master"><img alt="build status" src="https://gitlab.com/israeljbsilva/backend-test-to-do-list/badges/master/build.svg" /></a>
<a href="https://gitlab.com/israeljbsilva/backend-test-to-do-list/commits/master"><img alt="coverage report" src="https://gitlab.com/israeljbsilva/backend-test-to-do-list/badges/master/coverage.svg" /></a>


# Link da Aplicação no Heroku
- https://backend-test-to-do-list.herokuapp.com/api-docs/


# Link da Aplicação no Git
- https://gitlab.com/israeljbsilva/backend-test-to-do-list

# Ferramentas utilizadas:
- Python
- Django
- Postgres
- RabbitMQ
- Celery
- Git
- GitLab CI / CD


# Ferramenta de Automação:
Este projeto usa o `Makefile` como ferramenta de automação. 


# Ambiente Virtual de Configuração:
Os seguintes comandos instalam e configuram a ferramenta `pyenv` (https://github.com/pyenv/pyenv) usada para criar / gerenciar ambientes virtuais:

```bash
$ git clone https://github.com/pyenv/pyenv.git ~/.pyenv
$ echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
$ echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
$ echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n eval "$(pyenv init -)"\nfi' >> ~/.bashrc
$ exec "$SHELL"
$ git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv
$ echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bashrc
$ exec "$SHELL"
```

Depois disso, acesse o diretório do projeto e execute `make all` para recriar o ambiente virtual.

# Testes

## Executa testes no SQLite:
```
$ make test
```

## Convenção de Código:

## Executa as conveções e métricas no código:
```
$ make code-convention
```
 
# Documentação da API
A documentação da API está disponível no endpoint `/api-docs/`.


# Item 1 e 2 da prova
- 1- CRUD de TODO Lists: criar, remover, editar o título;
- 2- CRUD de tarefas: para cada TODO List, possibilidade de criar tarefas, editar, remover, marcar com concluída e adicionar uma deadline;

Para estes itens, foi criado as apis /to-do-list


# Item 3 da prova
- 3- Possibilidade de atribuir uma tarefa para um usuário;

Ao criar uma to-do-list, o usuário já deve ser atribuído a tarefa. Ou seja, deve-se criar primeiro o usuário.


# Item 4 da prova
- 4- Relatório diário via e-mail de tarefas concluídas naquele dia e próximas prioridades, baseado no deadline;

O relatório foi gerado em pdf.

Caso queiram testar este item localmente, basta executar o comando abaixo:
``` 
python manage.py process_tasks --log-std
```

# Item 5 da prova
- 5- Possibilidade de solicitar um relatório, enviando a data de início e fim, em um endpoint REST com autenticação utilizando token. A resposta do endpoint deve ser imediata, informando se é possível a geração do relatório e, se sim, para qual e-mail será enviado. A geração e envio via e-mail deve acontecer de forma assíncrona (em background);

O relatório foi gerado em pdf.

Foi criando o endpoint /report.

## Authenticação
Foi utilizado o JWT como forma de autenticação.
O JWT é adquirido trocando um nome de usuário + senha por um token de acesso.

O usuário e senha serão aqueles criados no endpoint /user/create-account

## /user-auth
- Foi criado o endpoint `/user-auth` para realizar a autenticação;
- Na resposta, será retornado o "access" (que é o token de autentecação);

## Authorization:
Via Swagger, pode ser inserido o token (access) através do botão `Authorize` (canto superior direito), informando a literaral "`Bearer + token`", conforme exemplo abaixo:
```
Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTQzODI4NDMxLCJqdGkiOiI3ZjU5OTdiNzE1MGQ0NjU3OWRjMmI0OTE2NzA5N2U3YiIsInVzZXJfaWQiOjF9.Ju70kdcaHKn1Qaz8H42zrOYk0Jx9kIckTn9Xx7vhikY
```

Ou via curl:
```
curl http://127.0.0.1:8000/api/v1/person -H 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTQzODI4NDMxLCJqdGkiOiI3ZjU5OTdiNzE1MGQ0NjU3OWRjMmI0OTE2NzA5N2U3YiIsInVzZXJfaWQiOjF9.Ju70kdcaHKn1Qaz8H42zrOYk0Jx9kIckTn9Xx7vhikY'
```

Caso queiram testar estes itens localmente, basta executar os comandos abaixo:
## Subir RabbitMQ para realizar testes referente ao item 5 da prova
```
docker-compose up
```
## Start do celery para realizar testes referente ao item 5 da prova 
```
celery -A backend_test_to_do_list worker -l info -P eventlet
```

# Item 6
- 6- Possibilidade de fazer login no usuário admin@biome-hub.com, onde seu password será xxyyzz onde xx será o dobro do dia atual, yy será o dobro do mês e zz a hora atual (sempre 6 caracteres);

Foi criado os endpoints /user.

OBS: 
- RESET DE SENHA NÃO FOI FINALIZADO
- A SENHA NÃO FOI CONFIGURADA CONFORNME SOLICITADO
- FALTOU INCLUIR TESTES NAS DEMAIS FUNÇÕES
